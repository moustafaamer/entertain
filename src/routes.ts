import { Express, Request, Response } from "express";
import activityController from "./controller/activity.controller";
import userController from "./controller/user.controller";
import userMiddleware from "./middleware/user.middleware";

export default function (app: Express) {
  app.get("/healthcheck", (req: Request, res: Response) => res.sendStatus(200));

  // GET /activity
  app.get("/activity", activityController.getActivityHandler);

  // POST /user
  app.post("/user", userMiddleware.ValidateRequiredUserBodyFields, userMiddleware.ValidateUserBodyTypes, userController.addUserHandler)
}