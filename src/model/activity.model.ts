export enum AccessibilityTier {
  High = "High",
  Medium = "Medium",
  Low = "Low"
}

export enum PriceTier {
  Free = "Free",
  Low = "Low",
  High = "High"
}

export interface IBoredApiResponse {
  activity: string;
  accessibility: number;
  type: string;
  participants: string;
  price: number;
  link: string;
  key: string;
}

export interface IActivity {
  activity: string;
  accessibility: AccessibilityTier;
  type: string;
  participants: string;
  price: PriceTier;
  link: string;
  key: string;
}

export interface IAccessibilityRange {
  min: number;
  max: number;
}

export interface IPriceRange {
  min: number;
  max: number;
}
