import { AccessibilityTier, PriceTier } from "./activity.model";

export interface IUser {
  name: string;
  accessibility: AccessibilityTier;
  price: PriceTier;
}