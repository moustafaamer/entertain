export enum AccessibilityTier {
  High = "High",
  Medium = "Medium",
  Low = "Low"
}

export enum PriceTier {
  Free = "Free",
  Low = "Low",
  High = "High"
}

export interface IActivity {
  activity: string;
  accessibility: AccessibilityTier;
  type: string;
  participants: string;
  price: PriceTier;
  link: string;
  key_prop: string; // Using key_prop since 'key' is a special prop in react
}

export interface IActivityResponse {
  activity: string;
  accessibility: AccessibilityTier;
  type: string;
  participants: string;
  price: PriceTier;
  link: string;
  key: string;
  error?: string;
}