import { PriceTier, AccessibilityTier } from "./Activity";

export interface IUser {
  name: string;
  price: PriceTier;
  accessibility: AccessibilityTier;
}