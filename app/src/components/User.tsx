import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { getPriceColor, getAccessibilityColor } from '../helpers/Activity';
import { IUser } from '../types/User';


function User(props: IUser) {
  return (
    <>
      <h2>User Added!</h2>
      <TableContainer component={Paper} style={{marginTop: '5%'}}>
          <Table sx={{ minWidth: 650 }}>
              <TableHead style={{backgroundColor: 'lightgray'}}>
                  <TableRow>
                      <TableCell><b>Name</b></TableCell>
                      <TableCell align='right'><b>Price</b></TableCell>
                      <TableCell align='right'><b>Accessibility</b></TableCell>
                  </TableRow>
              </TableHead>
              <TableBody>
                  <TableRow>
                      <TableCell>{props.name}</TableCell>
                      <TableCell style={{color: getPriceColor(props.price)}} align='right'>{props.price}</TableCell>
                      <TableCell style={{color: getAccessibilityColor(props.accessibility)}} align='right'>{props.accessibility}</TableCell>
                  </TableRow>
              </TableBody>
          </Table>
      </TableContainer>
    </>
  );
}

export default User;
