import React from 'react';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import { Activity, ActivityProps } from './Activity';
import { PriceTier, AccessibilityTier } from '../types/Activity';
import { IUser } from '../types/User';
import { userPostRequest, activityGetRequest } from '../helpers/Api';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import User from './User';


function UserForm() {
  const [name, setName] = React.useState<string>('');
  const [price, setPrice] = React.useState<PriceTier>(PriceTier.Free);
  const [accessibility, setAccessibility] = React.useState<AccessibilityTier>(AccessibilityTier.Low);
  const [user, setUser] = React.useState<IUser>();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [apiError, setApiError] = React.useState<boolean>(false);
  const [activityProps, setActivityProps] = React.useState<ActivityProps>();

  const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  const handlePriceChange = (event: SelectChangeEvent) => {
    setPrice(event.target.value as PriceTier);
  };

  const handleAccessibilityChange = (event: SelectChangeEvent) => {
    setAccessibility(event.target.value as AccessibilityTier);
  };

  const handleUserSubmit = async () => {
    const body: IUser = {
      name,
      price,
      accessibility
    };
    try {
      setLoading(true);
      const userResponse = await userPostRequest(body);
      const activityResponse = await activityGetRequest();
      if (activityResponse.error) throw new Error();
      setApiError(false);
      setUser(userResponse);
      const props: ActivityProps = {
        header: 'New activity!',
        activityObject: {
          activity: activityResponse.activity,
          accessibility: activityResponse.accessibility,
          type: activityResponse.type,
          participants: activityResponse.participants,
          price: activityResponse.price,
          link: activityResponse.link,
          key_prop: activityResponse.key
        }
      }
      setActivityProps(props);
      setLoading(false);
    } catch (err) {
      setApiError(true);
      setLoading(false);
    }
  };

  return (
    <div>
        <h2>Add User</h2>
        <TextField required label='Name' variant='standard' value={name} onChange={handleNameChange}></TextField>
        <FormControl style={{marginLeft: '50px', width: '100px'}}>
            <InputLabel>Price</InputLabel>
            <Select
              label='Price'
              value={price}
              onChange={handlePriceChange}
            >
              <MenuItem value={PriceTier.Free}>{PriceTier.Free}</MenuItem>
              <MenuItem value={PriceTier.Low}>{PriceTier.Low}</MenuItem>
              <MenuItem value={PriceTier.High}>{PriceTier.High}</MenuItem>
            </Select>
        </FormControl>
        <FormControl style={{marginLeft: '50px', width: '100px'}}>
            <InputLabel>Accessibility</InputLabel>
            <Select
              label='Accessibility'
              value={accessibility}
              onChange={handleAccessibilityChange}
            >
              <MenuItem value={AccessibilityTier.Low}>{AccessibilityTier.Low}</MenuItem>
              <MenuItem value={AccessibilityTier.Medium}>{AccessibilityTier.Medium}</MenuItem>
              <MenuItem value={AccessibilityTier.High}>{AccessibilityTier.High}</MenuItem>
            </Select>
        </FormControl>
        <Button style={{marginLeft: '50px', marginTop: '8px'}} variant='contained' onClick={handleUserSubmit}>Submit</Button>
        {user && activityProps && !apiError ? 
          <>
            <User {...user} />
            <Activity {...activityProps} />
          </>
          : null}
        {apiError ? <h4 style={{color: 'red'}}>Api Error</h4> : null}
        {loading ? <CircularProgress style={{marginLeft: '50px'}} /> : null}
    </div>
  );
}

export default UserForm;
