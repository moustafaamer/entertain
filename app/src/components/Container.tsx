import React, { useEffect } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import { Activity, ActivityProps } from './Activity';
import UserForm from './UserForm';
import { activityGetRequest } from '../helpers/Api';

function Container() {
  const [activityProps, setActivityProps] = React.useState<ActivityProps>();
  const [apiError, setApiError] = React.useState<boolean>(false);
  const [loading, setLoading] = React.useState<boolean>(false);

  const getActivity = async () => {
    try {
      setLoading(true);
      const activityResponse = await activityGetRequest();
      if (activityResponse.error) throw new Error();
      const props: ActivityProps = {
        header: "Today's activity!",
        activityObject: {
          activity: activityResponse.activity,
          accessibility: activityResponse.accessibility,
          type: activityResponse.type,
          participants: activityResponse.participants,
          price: activityResponse.price,
          link: activityResponse.link,
          key_prop: activityResponse.key
        }
      }
      setActivityProps(props);
      setApiError(false);
      setLoading(false);
    } catch (error) {
      setApiError(true);
      setLoading(false);
    }
  }

  useEffect(() => {
    getActivity();
  }, [])

  return (
    <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', marginTop: '5%'}}>
        {activityProps && !apiError ? <Activity {...activityProps}/> : null}
        {loading ? <CircularProgress /> : null}
        {apiError ? <h4 style={{color: 'red'}}>Error fetching activity</h4> : null}
        <UserForm />
    </div>
  );
}

export default Container;
