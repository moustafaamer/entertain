import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { IActivity } from '../types/Activity';
import { getPriceColor, getAccessibilityColor } from '../helpers/Activity';

export interface ActivityProps {
  activityObject: IActivity;
  header: string;
}

export function Activity({activityObject, header}: ActivityProps) {
  return (
    <>
      <h2>{header}</h2>
      <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }}>
              <TableHead style={{backgroundColor: 'lightgray'}}>
                  <TableRow>
                      <TableCell><b>Activity</b></TableCell>
                      <TableCell align='right'><b>Type</b></TableCell>
                      <TableCell align='right'><b>Participants</b></TableCell>
                      <TableCell align='right'><b>Price</b></TableCell>
                      <TableCell align='right'><b>Link</b></TableCell>
                      <TableCell align='right'><b>Key</b></TableCell>
                      <TableCell align='right'><b>Accessibility</b></TableCell>
                  </TableRow>
              </TableHead>
              <TableBody>
                  <TableRow>
                      <TableCell>{activityObject.activity}</TableCell>
                      <TableCell align='right'>{activityObject.type}</TableCell>
                      <TableCell align='right'>{activityObject.participants}</TableCell>
                      <TableCell style={{color: getPriceColor(activityObject.price)}} align='right'>{activityObject.price}</TableCell>
                      <TableCell align='right'>{activityObject.link ? <a href={activityObject.link}> {activityObject.link} </a> : 'N/A'}</TableCell>
                      <TableCell align='right'>{activityObject.key_prop}</TableCell>
                      <TableCell style={{color: getAccessibilityColor(activityObject.accessibility)}} align='right'>{activityObject.accessibility}</TableCell>
                  </TableRow>
              </TableBody>
          </Table>
      </TableContainer>
    </>
  );
}
