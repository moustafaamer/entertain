import { IUser } from "../types/User";
import {IActivityResponse} from "../types/Activity";

const SERVER_URL = process.env.REACT_APP_SERVER_URL || "";

export async function activityGetRequest(): Promise<IActivityResponse> {
  const response = await fetch(`${SERVER_URL}/activity`, {
    'method': 'GET',
    'headers': {
      'content-type': 'application/json',
    }
  });
  if (response.status !== 200) throw new Error();
  return response.json();
}

export async function userPostRequest(user: IUser) {
  const response = await fetch(`${SERVER_URL}/user`, {
    'method': 'POST',
    'headers': {
      'content-type': 'application/json',
    },
    'body': JSON.stringify(user)
  });

  if (response.status !== 200) throw new Error();
  return response.json();
}