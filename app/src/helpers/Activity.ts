import { PriceTier, AccessibilityTier } from "../types/Activity";

export const getPriceColor = (priceTier: PriceTier) => {
  if (priceTier === PriceTier.Free) {
      return "green";
  } else if (priceTier === PriceTier.Low) {
      return "gold";
  } else {
      return "red";
  }
};

export const getAccessibilityColor = (accessibilityTier: AccessibilityTier) => {
  if (accessibilityTier === AccessibilityTier.Low) {
      return "red";
  } else if (accessibilityTier === AccessibilityTier.Medium) {
      return "gold";
  } else {
      return "green";
  }
};